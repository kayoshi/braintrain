package com.example.braintrain;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.os.CountDownTimer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int randomA;
    int randomB;
    int fails;
    int points;
    CountDownTimer timer;
    int seconds;

    public void setText(String id, String text) {
        int resID = getResources().getIdentifier(id, "id", getPackageName());
        TextView textView = (TextView) findViewById(resID);
        textView.setText(text);
    }

    public int randomValue() {
        Random r = new Random();
        return r.nextInt(20) + 1;
    }

    public Button[] getGuessButtons() {
        Button guessA = (Button) findViewById(R.id.guessA);
        Button guessB = (Button) findViewById(R.id.guessB);
        Button guessC = (Button) findViewById(R.id.guessC);
        Button guessD = (Button) findViewById(R.id.guessD);
        Button[] guessButtons = {guessA, guessB, guessC, guessD};
        return guessButtons;
    }

    public void makeControlsVisible(boolean visible) {
        Button[] guessButtons = getGuessButtons();
        for(Button button : guessButtons) {
            button.setVisibility(visible ? Button.VISIBLE : Button.INVISIBLE);
        }
    }

    public void start(View view) {
        makeControlsVisible(true);
        points = 0; fails = 0; seconds = 30;
        redrawFails(); redrawPoints();
        startCountdown();
        setQuestion();
    }

    public void startCountdown() {
        if (timer != null) {
            timer.cancel();
        }

        timer = new CountDownTimer(seconds * 1000 + 100, 1000) {
            public void onTick(long millisLeft) {
                setText("secondsLeft", millisLeft / 1000 + "s");
            }

            public void onFinish() {
                handleGameOver();
            }
        };
        timer.start();
    }

    public void setQuestion() {
        randomA = randomValue();
        randomB = randomValue();

        setText("question", randomA + " + " + randomB);

        Button[] guessButtons = getGuessButtons();
        Random rand = new Random();
        Button randomButton = guessButtons[rand.nextInt(guessButtons.length)];

        for(Button button : guessButtons) {
            if (button == randomButton) {
                button.setText(Integer.toString(randomA + randomB));
            } else {
                int randomValue = rand.nextInt(41);
                button.setText(Integer.toString(randomValue));
            }
        }
    }

    public void redrawPoints() {
        setText("points", Integer.toString((points)) + " points");
    }

    public void redrawFails() {
        setText("fails", Integer.toString((fails)) + " fails");
    }

    public void handleGameOver() {
        setText("question", "Game over! \n" + points + " points");
        timer.cancel();
        makeControlsVisible(false);
    }

    public void checkGuess(View view) {
        Button button = (Button) view;
        String buttonText = button.getText().toString();

        if (Integer.parseInt(buttonText) == randomA + randomB) {
            points++;
            redrawPoints();
        } else {
            fails++;
            redrawFails();
            if (fails == 3) {
                handleGameOver();
                return;
            }
        }

        setQuestion();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
}
